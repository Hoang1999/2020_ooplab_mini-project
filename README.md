# README #

### What is this repository for? ###

* This repository is finished at 5/6/2020
* This repository is created as a MidTerm Mini-Project
* This repository visualizes Queue, ArrayList and HashTable Data Structure
* Version 1.0

### How do I get set up? ###

* This repository is built in:
	- Eclipse
	- javafx-sdk-11.0.2
	- openjdk-13+33_windows-x64_bin
	- javafx_scenebuilder-2_0-windows
* In file Environment built in/Set environment (javafx-sdk-11.0.2) I put 9 pictures as instruction to set environment of javafx-sdk-11.0.2

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

****************************--------------NOTICE-------------------************************************
Animation khai báo trong hàm không được thực hiện song song với các câu lệnh trong hàm
Mà animation sẽ được tổng hợp lại và đồng thời tất cả đc kích hoạt, không phân biệt khai báo sau hay trước
(Trừ trường hợp đưa các animation vào Sequence Animation hoặc Parallel Animation)

Dung animation thi cac gia tri mà Animation đó thay đổi (layout, transition...) khong duoc cap nhat ngay khi animation đang chạy!
Sau khi tat ca animation da ket thuc thi moi that su duoc cap nhat

Nếu được thì hãy thử dùng Timeline cho trường hợp như vậy
-------------------------------------******************-------------------------------------------------