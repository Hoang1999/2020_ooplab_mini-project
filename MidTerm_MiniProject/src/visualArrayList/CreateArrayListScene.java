package visualArrayList;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class CreateArrayListScene {
	public Scene createScene() throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("./ArrayList.fxml"));
		ArrayListController menuCont = new ArrayListController();
		loader.setController(menuCont);

		Parent root = loader.load();

		Scene scene = new Scene(root, 900, 600);

		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		return scene;
	}
}
