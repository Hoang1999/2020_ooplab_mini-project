package visualArrayList;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ArrayListController implements Initializable {
	@FXML
	Button backButton;
	@FXML
	private BorderPane borderPane;
	@FXML
	private AnchorPane functionPane;
	@FXML
	private AnchorPane contentPane;
	@FXML
	TextField addValue;
	@FXML
	TextField insertValue;
	@FXML
	TextField insertIndex;
	@FXML
	TextField deleteIndex;

	public ArrayList<String> items = new ArrayList<String>();
	public ArrayList<StackPane> rectangles = new ArrayList<StackPane>();
	final static int BASE_X = 25;
	final static int BASE_Y = 280;
	final static int BASE__INDEX_X = 59;
	final static int BASE_INDEX_Y = 340;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	public void back() {
		// borderPane.getChildren().setAll();
	}

	// Check input and do "adding"
	public void add(ActionEvent event) {
		String value = addValue.getText();
		addValue.clear();
		if (value.length() == 0) {
			addAlert();
		} else {
			addAnimation(value);
		}
	}

	// Check input and do "inserting"
	public void insert(ActionEvent event) {
		String value = insertValue.getText();
		String indexStr = insertIndex.getText();
		if (value.length() == 0) {
			addAlert();
		} else if (indexStr.length() == 0) {
			insertIndexAlert();
		} else {
			int index = Integer.parseInt(indexStr);
			if (index > rectangles.size()) {
				insertIndexAlert();
			} else {
				insertAnimation(index, value);

			}
			insertValue.clear();
			insertIndex.clear();
		}
	}

	// Check input and do "removing"
	public void delete(ActionEvent event) {
		String indexStr = deleteIndex.getText();
		if (indexStr.length() == 0) {
			deleteIndexAlert();
		} else {
			int index = Integer.parseInt(indexStr);
			if (index >= rectangles.size()) {
				deleteIndexAlert();
			} else {
				deleteAnimation(index);
			}
		}
		deleteIndex.clear();
	}

	// Adding a "box" node into array list
	public void addRectangle(int index, String value) {
		// Creating a box
		Rectangle node = new Rectangle();
		node.setHeight(46);
		node.setWidth(77);
		node.setFill(Color.web("#a5ecf4"));
		node.setStroke(Color.web("#58a0a8"));

		// Creating a value inside the box
		Text textValue = new Text(value);
		StackPane stp = new StackPane();
		stp.getChildren().addAll(node, textValue);
		stp.setLayoutX(BASE_X + index * 100);
		stp.setLayoutY(BASE_Y);

		rectangles.add(index, stp);
	}

	// Do "adding"
	public void addAnimation(String value) {
		// Creating a fake box
		Rectangle node = new Rectangle();
		node.setHeight(46);
		node.setWidth(77);
		node.setFill(Color.web("#A6F87E"));
		node.setStroke(Color.web("#84c664"));

		// Set value for this fake box
		Text textValue = new Text(value);

		// Make these 2 into 1 thing (box with value)
		StackPane stp = new StackPane();
		stp.getChildren().addAll(node, textValue);
		contentPane.getChildren().add(stp);

		// New box flying into the newest position
		TranslateTransition translate = new TranslateTransition();
		translate.setFromX(45);
		translate.setFromY(-65);
		translate.setToX(BASE_X + rectangles.size() * 100);
		translate.setToY(BASE_Y);
		translate.setDuration(Duration.millis(300));
		translate.setNode(stp);

		// Make it feel more "appearing-displaying"
		FadeTransition fade = new FadeTransition();
		fade.setFromValue(.1);
		fade.setToValue(10);
		fade.setDuration(Duration.millis(300));

		// Make these two animations display together
		ParallelTransition appearIn = new ParallelTransition(stp, translate, fade);

		// After the animation, add "real" box and add value into array list then
		// display them again
		appearIn.setOnFinished(e -> {
			addRectangle(rectangles.size(), value);
			items.add(value);

			// Clear all then display again
			contentPane.getChildren().clear();
			display();
		});
		appearIn.play();
	}

	// Do "inserting"
	public void insertAnimation(int index, String value) {

		// Move the nodes after "insert-position" to next position
		ParallelTransition moveNext = new ParallelTransition();
		for (int i = index; i < rectangles.size(); i++) {
			TranslateTransition translate = new TranslateTransition();
			translate.setNode(rectangles.get(i));
			translate.setByX(100);
			moveNext.getChildren().add(translate);
		}
		moveNext.play();

		// Inserting new box into needed position
		// // Creating a fake box
		Rectangle node = new Rectangle();
		node.setHeight(46);
		node.setWidth(77);
		node.setFill(Color.web("#ffff66"));
		node.setStroke(Color.web("#e5e55b"));

		// Set value for this fake box
		Text textValue = new Text(value);

		// Make these 2 into 1 thing (box with value)
		StackPane stp = new StackPane();
		stp.getChildren().addAll(node, textValue);
		contentPane.getChildren().add(stp);

		// New box flying into the needed position
		TranslateTransition translate1 = new TranslateTransition();
		translate1.setFromX(220);
		translate1.setFromY(-65);
		translate1.setToX(BASE_X + index * 100);
		translate1.setToY(BASE_Y);
		translate1.setDuration(Duration.millis(300));
		translate1.setNode(stp);

		// Make it feel more "appearing-displaying"
		FadeTransition fade = new FadeTransition();
		fade.setFromValue(.1);
		fade.setToValue(10);
		fade.setDuration(Duration.millis(300));

		// Make these two animations display together
		ParallelTransition appearIn = new ParallelTransition(stp, translate1, fade);

		// After the animation, add "real" box and add value into array list then
		// display them again
		appearIn.setOnFinished(e -> {
			addRectangle(index, value);
			items.add(index, value);

			// Clear all then display again
			contentPane.getChildren().clear();
			display();
		});
		appearIn.play();

	}

	// Do "removing"
	public void deleteAnimation(int index) {

		// Make the removed node fly away
		TranslateTransition translate1 = new TranslateTransition();
		translate1.setToX(1000);
		translate1.setToY(1000);
		translate1.setDuration(Duration.millis(500));

		// Make the flying node fade while flying away
		FadeTransition fade = new FadeTransition();
		fade.setFromValue(10);
		fade.setToValue(0);
		fade.setDuration(Duration.millis(200));

		// Make these two animation display together
		ParallelTransition fadeAway = new ParallelTransition(rectangles.get(index), translate1, fade);

		// After the animation, add "real" box and add value into array list then
		// display them again
		fadeAway.setOnFinished(e -> {
			rectangles.remove(index);
			items.remove(index);

			// Clear all then display again
			contentPane.getChildren().clear();
			display();

		});
		fadeAway.play();

		// For some reasons, i need to move nodes after the removed node to their front
		ParallelTransition movePrev = new ParallelTransition();
		for (int i = index; i < rectangles.size(); i++) {
			TranslateTransition translate = new TranslateTransition();
			translate.setNode(rectangles.get(i));
			translate.setByX(-100);
			movePrev.getChildren().add(translate);
		}
		movePrev.play();
	}

	// Display all nodes after animations
	public void display() {
		int i = 0;
		for (i = 0; i < rectangles.size(); i++) {
			Label index = new Label();
			index.setText(String.valueOf(i));
			index.setLayoutX(BASE__INDEX_X + i * 100);
			index.setLayoutY(BASE_INDEX_Y);

			contentPane.getChildren().add(rectangles.get(i));
			contentPane.getChildren().add(index);
		}
	}

	// Show alert when user interact with functions

	public void addAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Input Alert");
		alert.setHeaderText(null);
		alert.setContentText("You must add a value into array list!");
		alert.showAndWait();
	}

	public void insertIndexAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Input Alert");
		alert.setHeaderText(null);
		alert.setContentText("Wrong index! Index must be given or be smaller than or equal to array list's size!");
		alert.showAndWait();
	}

	public void deleteIndexAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Input Alert");
		alert.setHeaderText(null);
		alert.setContentText("Wrong index! Index must be smaller than array list's size!");
		alert.showAndWait();
	}
}