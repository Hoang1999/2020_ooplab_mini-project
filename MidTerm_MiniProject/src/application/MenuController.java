package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import visualArrayList.CreateArrayListScene;
import visualHashTable.CreateHashTableScene;
import visualQueue.CreateScene;

public class MenuController implements Initializable {
	Stage stage;

	@FXML
	BorderPane borderPane;

	/*
	 * private double xOffset = 0; private double yOffset = 0;
	 */

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Not used
	}

	public void switchToQueue(ActionEvent event) {
		try {
			Scene queueScene = CreateScene.getScene();
			borderPane.setCenter(queueScene.getRoot());

			/*
			 * primaryStage.setScene(queueScene); queueScene.getRoot().setOnMousePressed(new
			 * EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) { xOffset = event.getSceneX();
			 * yOffset = event.getSceneY(); } }); queueScene.getRoot().setOnMouseDragged(new
			 * EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) {
			 * primaryStage.setX(event.getScreenX() - xOffset);
			 * primaryStage.setY(event.getScreenY() - yOffset); } });
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void switchToArrayList(ActionEvent event) {
		try {
			Scene arrayListScene = new CreateArrayListScene().createScene();
			borderPane.setCenter(arrayListScene.getRoot());

			/*
			 * primaryStage.setScene(arrayListScene);
			 * primaryStage.setTitle("Midterm Project: Array List");
			 * 
			 * arrayListScene.getRoot().setOnMousePressed(new EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) { xOffset = event.getSceneX();
			 * yOffset = event.getSceneY(); } });
			 * arrayListScene.getRoot().setOnMouseDragged(new EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) {
			 * primaryStage.setX(event.getScreenX() - xOffset);
			 * primaryStage.setY(event.getScreenY() - yOffset); } });
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void switchToHashTable(ActionEvent event) {
		System.out.println("Start");
		try {
			Scene hashTableScene = new CreateHashTableScene().createScene();
			borderPane.setCenter(hashTableScene.getRoot());

			/*
			 * primaryStage.setScene(hashTableScene);
			 * primaryStage.setTitle("Midterm Project: Hash Table");
			 * 
			 * hashTableScene.getRoot().setOnMousePressed(new EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) { xOffset = event.getSceneX();
			 * yOffset = event.getSceneY(); } });
			 * hashTableScene.getRoot().setOnMouseDragged(new EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) {
			 * primaryStage.setX(event.getScreenX() - xOffset);
			 * primaryStage.setY(event.getScreenY() - yOffset); } });
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void exit() {
		System.exit(0);
	}

	public Stage getPrimaryStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
