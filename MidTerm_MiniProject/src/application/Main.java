package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	private String menuFXML = "Menu.fxml";
	private String stageIcon = "./../visualQueue/Images/cute-05.png";

	private Parent menuRoot;

	/*
	 * Used to make undecorated stage private double xOffset = 0; private double
	 * yOffset = 0;
	 */

	@Override
	public void start(Stage primaryStage) {

		try {
			// set up for primaryStage
			primaryStage.setTitle("Data Structure Visualization");
			primaryStage.getIcons().add(new Image(getClass().getResourceAsStream(stageIcon)));

			/*
			 * Used to make undecorated stage
			 * primaryStage.initStyle(StageStyle.UNDECORATED);
			 */

			FXMLLoader menuLoader = new FXMLLoader(getClass().getResource(menuFXML));
			MenuController menuCont = new MenuController();
			menuLoader.setController(menuCont);

			Scene menuScene;
			try {
				menuRoot = menuLoader.load();
				menuScene = new Scene(menuRoot);

				// Put the scene to the primaryStage

				// Or test scene
				/* menuScene = new CreateArrayListScene().createScene(); */

				primaryStage.setScene(menuScene);

				menuCont.setStage(primaryStage);
			} catch (Exception e) {
				e.printStackTrace();
			}

			/*
			 * Used to make undecorated stage menuRoot.setOnMousePressed(new
			 * EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) { xOffset = event.getSceneX();
			 * yOffset = event.getSceneY(); } }); menuRoot.setOnMouseDragged(new
			 * EventHandler<MouseEvent>() {
			 * 
			 * @Override public void handle(MouseEvent event) {
			 * primaryStage.setX(event.getScreenX() - xOffset);
			 * primaryStage.setY(event.getScreenY() - yOffset); } });
			 */

			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}