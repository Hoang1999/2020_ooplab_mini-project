package visualQueue;

import javafx.scene.Scene;

public class CreateScene {
	private static final Scene scene;

	static {
		scene = new Scene(new CreateRoot().getRoot());
	}

	public static Scene getScene() {
		return scene;
	}
}
