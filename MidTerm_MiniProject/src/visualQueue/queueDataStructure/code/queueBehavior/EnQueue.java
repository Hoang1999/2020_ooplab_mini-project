package visualQueue.queueDataStructure.code.queueBehavior;

import visualQueue.queueDataStructure.code.Queue;

public class EnQueue extends QueueBehavior {
	public EnQueue(Queue queue) {
		super(queue);
	}

	private int newNodeID;

	public void enQueue(String value) {
		System.out.println("---------------------------------------------------------");
		System.out.println("Adding new node to Queue.\nNode value: " + value);
		queue.getQueueNodeList().put(queue.getRear() + 1, value);
		newNodeID = queue.getRear() + 1;
		queue.setRear(queue.getRear() + 1);
		if (queue.getFront() == 0)
			queue.setFront(queue.getRear());
		System.out.println("Node ID (New node): " + queue.getRear());
		System.out.println("---------------------------------------------------------\n");
	}

	public int getNewNodeID() {
		return newNodeID;
	}

}
