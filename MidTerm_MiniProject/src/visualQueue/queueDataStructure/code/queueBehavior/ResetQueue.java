package visualQueue.queueDataStructure.code.queueBehavior;

import visualQueue.queueDataStructure.code.Queue;

public class ResetQueue extends QueueBehavior {
	public ResetQueue(Queue queue) {
		super(queue);
	}

	public void reset() {
		queue.getQueueNodeList().clear();
		queue.setFront(0);
		queue.setRear(0);
	}
}
