package visualQueue.queueDataStructure.code.queueBehavior;

import visualQueue.queueDataStructure.code.Queue;

public class DeQueue extends QueueBehavior {
	public DeQueue(Queue queue) {
		super(queue);
	}

	public String deQueue() {
		System.out.println("---------------------------------------------------------");
		String deQueueValue = queue.getQueueNodeList().get(queue.getFront());
		System.out.println("Start DeQueue...\nNode value: " + deQueueValue);
		queue.getQueueNodeList().remove(queue.getFront());
		queue.setFront(queue.getFront() + 1);
		System.out.println("Node ID (New Front node): " + queue.getFront());
		System.out.println("---------------------------------------------------------\n");
		return deQueueValue;
	}
}
