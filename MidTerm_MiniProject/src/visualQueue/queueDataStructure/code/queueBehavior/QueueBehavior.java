package visualQueue.queueDataStructure.code.queueBehavior;

import visualQueue.queueDataStructure.code.Queue;

public class QueueBehavior {

	public final Queue queue;

	public QueueBehavior(Queue queue) {
		this.queue = queue;
	}
}
