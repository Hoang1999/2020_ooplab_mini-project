package visualQueue.queueDataStructure.code;

import java.util.HashMap;

import visualQueue.queueDataStructure.code.queueBehavior.DeQueue;
import visualQueue.queueDataStructure.code.queueBehavior.EnQueue;
import visualQueue.queueDataStructure.code.queueBehavior.ResetQueue;

public class Queue {
	private final DeQueue deQueue = new DeQueue(this);
	private final EnQueue enQueue = new EnQueue(this);
	private final ResetQueue resetQueue = new ResetQueue(this);

	private HashMap<Integer, String> queueNodeList = new HashMap<Integer, String>();

	private int front = 0;
	private int rear = 0;

	public int getNumOfNode() {
		return queueNodeList.size();
	}

	public HashMap<Integer, String> getQueueNodeList() {
		return queueNodeList;
	}

	public int getFront() {
		return front;
	}

	public void setFront(int front) {
		this.front = front;
	}

	public void setRear(int rear) {
		this.rear = rear;
	}

	public int getRear() {
		return rear;
	}

	public DeQueue getDeQueue() {
		return deQueue;
	}

	public EnQueue getEnQueue() {
		return enQueue;
	}

	public ResetQueue getResetQueue() {
		return resetQueue;
	}
}
