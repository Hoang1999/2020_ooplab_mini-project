package visualQueue.queueDataStructure.performent.queueBehaviorPerforment;

import java.util.HashMap;

import arrowLineManager.ArrowLinePositionPropertyCaculate;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.util.Duration;
import visualQueue.shape.queueNode.QueueNodeManager;

//SOLUTION: DEFINE A NEW ANIMATION
public class MoveQueueNode extends Transition {
	private final QueueNodeManager node;

	private final double destinationX;
	private final double destinationY;
	private final double[][] pos;

	public MoveQueueNode(QueueNodeManager node, Duration duration, double destinationX, double destinationY,
			double[][] pos) {
		this.node = node;
		this.destinationX = destinationX;
		this.destinationY = destinationY;
		this.pos = pos;

		setCycleDuration(duration);
		setInterpolator(Interpolator.LINEAR);
	}

	@Override
	protected void interpolate(double frec) {
		final double startNodeLayoutX = node.getQueueNode().getLayoutX();
		final double startNodeLayoutY = node.getQueueNode().getLayoutY();

		final double translationDegreeX = destinationX - startNodeLayoutX;
		final double translationDegreeY = destinationY - startNodeLayoutY;

		double currentNodeLayoutX = startNodeLayoutX + translationDegreeX * frec;
		double currentNodeLayoutY = startNodeLayoutY + translationDegreeY * frec;

		node.getQueueNode().setLayoutX(currentNodeLayoutX);
		node.getQueueNode().setLayoutY(currentNodeLayoutY);

		for (int i = 0; i < node.getArrowLineLinkCome().size(); i++) {
			HashMap<String, Double> newPositionProperty = new ArrowLinePositionPropertyCaculate().propertyCaculate(
					node.getArrowLineLinkCome().get(i).getFootLayoutX(),
					node.getArrowLineLinkCome().get(i).getFootLayoutY(), node.getQueueNode().getLayoutX() + pos[i][0],
					node.getQueueNode().getLayoutY() + pos[i][1]);

			node.getArrowLineLinkCome().get(i).getArrowLine().setRotate(newPositionProperty.get("angle"));
			node.getArrowLineLinkCome().get(i).getLine()
					.setEndX(node.getArrowLineLinkCome().get(i).lengthOfLine(newPositionProperty.get("length")));
		}
	}
}

/*
 * //ANOTHER SOLUTION USING TIMELINE class MoveHeadAlongLineUsingTimeLine {
 * private ArrowLineManager arrowLine;
 * 
 * private double destination_headLayoutX; private double
 * destination_headLayoutY;
 * 
 * private double footLayoutX; private double footLayoutY;
 * 
 * private double duration; private int flatDegree;
 * 
 * private Timeline moveHeadAlongTheLineTimeline = new Timeline();
 * 
 * public MoveHeadAlongLineUsingTimeLine(ArrowLineManager arrowLine, double
 * destination_headLayoutX, double destination_headLayoutY, double duration, int
 * flatDegree) { this.arrowLine = arrowLine;
 * 
 * this.destination_headLayoutX = destination_headLayoutX;
 * this.destination_headLayoutY = destination_headLayoutY;
 * 
 * this.footLayoutX = arrowLine.getFootLayoutX(); this.footLayoutY =
 * arrowLine.getFootLayoutY();
 * 
 * this.duration=duration; this.flatDegree=flatDegree;
 * 
 * this.timelineCaculate(); }
 * 
 * private void timelineCaculate() { double currentHeadLayoutX =
 * arrowLine.getHeadLayoutX(); double currentHeadLayoutY =
 * arrowLine.getHeadLayoutY();
 * 
 * double translationDegreeX=destination_headLayoutX - currentHeadLayoutX;
 * double translationDegreeY=destination_headLayoutY - currentHeadLayoutY;
 * 
 * int fractionX = (int) (translationDegreeX / flatDegree); int fractionY =
 * (int) (translationDegreeY / flatDegree); double durationFraction =
 * duration/flatDegree;
 * 
 * double presentDuration=0; int i=0;
 * 
 * while((currentHeadLayoutX!=destination_headLayoutX)||(currentHeadLayoutY!=
 * destination_headLayoutY)) { i=i+1;
 * 
 * if(i>flatDegree) { currentHeadLayoutX=destination_headLayoutX;
 * currentHeadLayoutY=destination_headLayoutY; }else { currentHeadLayoutX =
 * currentHeadLayoutX+fractionX; currentHeadLayoutY =
 * currentHeadLayoutY+fractionY; } presentDuration =
 * presentDuration+durationFraction;
 * 
 * HashMap<String, Double> newPositionProperty = new
 * ArrowLinePositionPropertyCaculate() .propertyCaculate(footLayoutX,
 * footLayoutY, currentHeadLayoutX, currentHeadLayoutY);
 * 
 * KeyValue keyValueAngle = new
 * KeyValue(arrowLine.getArrowLine().rotateProperty(),
 * newPositionProperty.get("angle")); KeyValue keyValueLineLength = new
 * KeyValue(arrowLine.getLine().endXProperty(),
 * arrowLine.lengthOfLine(newPositionProperty.get("length")));
 * 
 * moveHeadAlongTheLineTimeline.getKeyFrames().addAll(new KeyFrame(new
 * Duration(presentDuration), keyValueAngle, keyValueLineLength)); } }
 * 
 * public Timeline getMoveHeadAlongTheLineTimeline() { return
 * moveHeadAlongTheLineTimeline; } }
 */