package visualQueue.queueDataStructure.performent.queueBehaviorPerforment;

import arrowLineManager.ArrowLineManager;
import visualQueue.queueDataStructure.performent.QueuePerform;
import visualQueue.shape.queueNode.QueueNodeManager;

public class MakeArrowLink extends QueueBehaviorPerform {

	public MakeArrowLink(QueuePerform queuePer) {
		super(queuePer);
	}

	public void create(QueueNodeManager nodeLinkFrom, QueueNodeManager nodeLinkTo) {
		ArrowLineManager newArrowLink = new ArrowLineManager();
		newArrowLink.setStrokeWidth(1);
		newArrowLink.setArrowScale(0.1, 0.1);

		nodeLinkFrom.setArrowLineLinkTo(newArrowLink);
		nodeLinkTo.getArrowLineLinkCome().add(newArrowLink);

		double footLayoutX = nodeLinkFrom.getQueueNode().getLayoutX() + nodeLinkFrom.getQueueNode().getPrefWidth() - 10;
		double footLayoutY = nodeLinkFrom.getQueueNode().getLayoutY() + nodeLinkFrom.getQueueNode().getPrefHeight() / 2;

		double headLayoutX = nodeLinkTo.getQueueNode().getLayoutX();
		double headLayoutY = nodeLinkTo.getQueueNode().getLayoutY() + nodeLinkTo.getQueueNode().getPrefHeight() / 2;

		newArrowLink.setPosition(footLayoutX, footLayoutY, headLayoutX, headLayoutY);

		nodeLinkFrom.getNodeCont().getStrike1().setVisible(false);
		nodeLinkFrom.getNodeCont().getStrike2().setVisible(false);
	}
}
