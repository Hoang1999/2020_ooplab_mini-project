package visualQueue.queueDataStructure.performent.queueBehaviorPerforment;

import arrowLineAnimation.ArrowHeadMoveToANewPosition;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import visualQueue.queueDataStructure.performent.QueuePerform;
import visualQueue.shape.queueNode.QueueNodeManager;

public class DeQueuePerform extends QueueBehaviorPerform {

	public DeQueuePerform(QueuePerform queuePer) {
		super(queuePer);
	}

	private QueueNodeManager deNode;
	private String deQueueValue;

	public void deQueue() {
		deQueueValue = queuePer.getQueue().getDeQueue().deQueue();

		deNode = queuePer.getQueueNodeList().get(queuePer.getQueue().getFront() - 1);
		queuePer.getRemoveQueueNodeList().add(queuePer.getQueue().getFront() - 1);

		deNode.getQueueNode().getStyleClass().remove("hboxNodeNormal");
		deNode.getQueueNode().getStyleClass().add("hboxNodeDenode");

		if (queuePer.getQueue().getFront() - 1 == queuePer.getQueue().getRear())
			configVisualQueueOneNode();
		else
			configVisualQueue();
	}

	private void configVisualQueueOneNode() {
		// ------------------------------------- SETTING FOR ANIMATION
		// -------------------------------------

		// ------------------------------------- CREATE ANIMATION
		// -------------------------------------
		SequentialTransition enQueueTransition = new SequentialTransition();

		// STEP 1: MOVE THE ENQUEUE CIRCLE TO TAKE BACK VALUE FROM THE NEW
		// NODE----------------------------------------------
		Timeline enQueueCircleToRed = new Timeline();
		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getDeQueueCircle().strokeProperty(), Color.RED)));

		double beforeX = queuePer.getDeQueueCircle().getLayoutX();
		double beforeY = queuePer.getDeQueueCircle().getLayoutY();
		double posX = queuePer.getDeQueueValue().getLayoutX() - beforeX;
		double posY = queuePer.getDeQueueValue().getLayoutY() - beforeY;

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueCircle().layoutXProperty(),
								deNode.getQueueNode().getLayoutX() + 25)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueCircle().layoutYProperty(),
								deNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
						new KeyValue(queuePer.getDeQueueCircle().layoutXProperty(), beforeX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
						new KeyValue(queuePer.getDeQueueCircle().layoutYProperty(), beforeY)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueValue().layoutXProperty(),
								deNode.getQueueNode().getLayoutX() + 25 + posX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueValue().layoutYProperty(),
								deNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2 + posY)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
						new KeyValue(queuePer.getDeQueueValue().layoutXProperty(), beforeX + posX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
						new KeyValue(queuePer.getDeQueueValue().layoutYProperty(), beforeY + posY)));

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueValue().textProperty(),
								deNode.getNodeCont().getValueText().getText())),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(deNode.getNodeCont().getValueText().textProperty(), "")));

		// STEP 2: THE REAR AND FRONT UNPOINT TO THE NEW NODE
		// --------------------------------------------------------------------
		ArrowHeadMoveToANewPosition frontPointDel = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				queuePer.getDuration(), deNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				deNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight,
				queuePer.getFront().getFrontArrow().getFootLayoutX(),
				queuePer.getFront().getFrontArrow().getFootLayoutY());
		frontPointDel.setOnFinished(e -> {
			queuePer.getFront().getFrontArrow().getArrowLine().setVisible(false);
		});

		ArrowHeadMoveToANewPosition rearPointDel = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				queuePer.getDuration(), deNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				deNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight,
				queuePer.getRear().getRearArrow().getFootLayoutX(), queuePer.getRear().getRearArrow().getFootLayoutY());
		rearPointDel.setOnFinished(e -> {
			queuePer.getRear().getRearArrow().getArrowLine().setVisible(false);
		});

		// STEP 3: FADE THE NODE ~ DELETE THE NODE (NOT ACTUALLY DELETE, WE WILL DELETE
		// IT IN THE NEXT ENQUEUE ACTION)---------------------
		FadeTransition fade = new FadeTransition();
		fade.setDuration(new Duration(queuePer.getDuration().toMillis()));

		fade.setFromValue(10);
		fade.setToValue(0);
		fade.setNode(deNode.getQueueNode());

		// STEP 4: SET UP DEQUEUE CIRCLE TO NORMAL ~ DEQUEUE FINISHED
		// -------------------------------------------------
		Timeline enQueueCircleToBlack = new Timeline();
		enQueueCircleToBlack.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueCircle().strokeProperty(), Color.BLACK)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueValue().textProperty(), " ")));

		// ADDING ANIMATION INTO SEQUENCE TRANSITION----------------------------------
		enQueueTransition.getChildren().add(enQueueCircleToRed);
		enQueueTransition.getChildren().add(frontPointDel);
		enQueueTransition.getChildren().add(rearPointDel);
		enQueueTransition.getChildren().add(fade);
		enQueueTransition.getChildren().add(enQueueCircleToBlack);

		enQueueTransition.setOnFinished(e -> {
			queuePer.getEnQueueButton().setDisable(false);
			queuePer.getDeQueueButton().setDisable(false);
			queuePer.getResetButton().setDisable(false);
			// when sorting complete the randomButton are set to can be activated
		});

		enQueueTransition.play();
	}

	private void configVisualQueue() {
		// ------------------------------------- SETTING FOR ANIMATION
		// -------------------------------------
		QueueNodeManager almostFirstNode = queuePer.getQueueNodeList().get(queuePer.getQueue().getFront());

		deNode.getArrowLineLinkCome().remove(queuePer.getFront().getFrontArrow());

		almostFirstNode.getArrowLineLinkCome().remove(deNode.getArrowLineLinkTo());

		// ------------------------------------- CREATE ANIMATION
		// -------------------------------------
		SequentialTransition enQueueTransition = new SequentialTransition();

		// STEP 1: MOVE THE ENQUEUE CIRCLE TO TAKE BACK VALUE FROM THE NEW
		// NODE----------------------------------------------
		Timeline enQueueCircleToRed = new Timeline();
		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getDeQueueCircle().strokeProperty(), Color.RED)));

		double beforeX = queuePer.getDeQueueCircle().getLayoutX();
		double beforeY = queuePer.getDeQueueCircle().getLayoutY();
		double posX = queuePer.getDeQueueValue().getLayoutX() - beforeX;
		double posY = queuePer.getDeQueueValue().getLayoutY() - beforeY;

		enQueueCircleToRed
				.getKeyFrames().addAll(
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueCircle().layoutXProperty(),
										queuePer.getLeftSpace() + 25)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueCircle().layoutYProperty(),
										queuePer.getTopSpace() + QueueNodeManager.nodeHeight / 2)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
								new KeyValue(queuePer.getDeQueueCircle().layoutXProperty(), beforeX)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
								new KeyValue(queuePer.getDeQueueCircle().layoutYProperty(), beforeY)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueValue().layoutXProperty(),
										queuePer.getLeftSpace() + 25 + posX)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueValue().layoutYProperty(),
										queuePer.getTopSpace() + QueueNodeManager.nodeHeight / 2 + posY)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
								new KeyValue(queuePer.getDeQueueValue().layoutXProperty(), beforeX + posX)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
								new KeyValue(queuePer.getDeQueueValue().layoutYProperty(), beforeY + posY)));

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueValue().textProperty(),
								deNode.getNodeCont().getValueText().getText())),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(deNode.getNodeCont().getValueText().textProperty(), "")));

		// STEP 2: UPDATE THE FRONT ARROW
		// ---------------------------------------------------------------------
		double updateFront_1_headDesX = queuePer.getLeftSpace() + QueueNodeManager.nodeWidth / 2;
		double updateFront_1_headDesY = queuePer.getTopSpace() + QueueNodeManager.nodeHeight / 2;

		ArrowHeadMoveToANewPosition updateFront_1 = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				queuePer.getDuration().divide(4), deNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				queuePer.getTopSpace() + QueueNodeManager.nodeHeight, updateFront_1_headDesX, updateFront_1_headDesY);

		double updateFront_2_headDesX = almostFirstNode.getQueueNode().getLayoutX();
		double updateFront_2_headDesY = almostFirstNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2;

		ArrowHeadMoveToANewPosition updateFront_2 = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				queuePer.getDuration().divide(4), updateFront_1_headDesX, updateFront_1_headDesY,
				updateFront_2_headDesX, updateFront_2_headDesY);

		double updateFront_3_headDesX = almostFirstNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2;
		double updateFront_3_headDesY = queuePer.getTopSpace() + QueueNodeManager.nodeHeight;

		ArrowHeadMoveToANewPosition updateFront_3 = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				queuePer.getDuration().divide(4), updateFront_2_headDesX, updateFront_2_headDesY,
				updateFront_3_headDesX, updateFront_3_headDesY);

		// STEP 3: FADE THE NODE AND ITS POINT-TO ARROW ~ DELETE THE NODE (NOT ACTUALLY
		// DELETE, WE WILL DELETE IT IN THE NEXT ENQUEUE ACTION)---------------------
		Timeline fade = new Timeline();
		fade.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(3),
						new KeyValue(deNode.getQueueNode().opacityProperty(), 0)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(deNode.getArrowLineLinkTo().getArrowLine().opacityProperty(), 0)));

		// STEP 4: MODIFY THE LAYOUT OF THE QUEUE
		ParallelTransition modifyLayout = new ParallelTransition();
		for (int i = queuePer.getQueue().getFront(); i <= queuePer.getQueue().getRear(); i++) {
			Timeline translateNode = new Timeline();
			translateNode.getKeyFrames()
					.addAll(new KeyFrame(queuePer.getDuration(),
							new KeyValue(queuePer.getQueueNodeList().get(i).getQueueNode().layoutXProperty(),
									queuePer.getQueueNodeList().get(i).getQueueNode().getLayoutX()
											- (QueueNodeManager.nodeWidth + queuePer.getSpaceBetweenNode()))));
			modifyLayout.getChildren().add(translateNode);

			if (queuePer.getQueueNodeList().get(i).getArrowLineLinkTo() != null) {
				TranslateTransition translateArrow = new TranslateTransition();
				translateArrow.setInterpolator(Interpolator.LINEAR);
				translateArrow.setDuration(queuePer.getDuration());
				translateArrow.setByX(-(QueueNodeManager.nodeWidth + queuePer.getSpaceBetweenNode()));
				translateArrow.setNode(queuePer.getQueueNodeList().get(i).getArrowLineLinkTo().getArrowLine());
				modifyLayout.getChildren().add(translateArrow);
			}
		}
		ArrowHeadMoveToANewPosition moveFront = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				queuePer.getDuration(), updateFront_3_headDesX, updateFront_3_headDesY,
				queuePer.getLeftSpace() + QueueNodeManager.nodeWidth / 2,
				queuePer.getTopSpace() + QueueNodeManager.nodeHeight);
		modifyLayout.getChildren().add(moveFront);

		double startMoveRearX = queuePer.getQueueNodeList().get(queuePer.getQueue().getRear()).getQueueNode()
				.getLayoutX() + QueueNodeManager.nodeWidth / 2;
		double startMoveRearY = queuePer.getQueueNodeList().get(queuePer.getQueue().getRear()).getQueueNode()
				.getLayoutY() + QueueNodeManager.nodeHeight;

		ArrowHeadMoveToANewPosition moveRear = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				queuePer.getDuration(), startMoveRearX, startMoveRearY,
				startMoveRearX - (QueueNodeManager.nodeWidth + queuePer.getSpaceBetweenNode()), startMoveRearY);
		modifyLayout.getChildren().add(moveRear);

		// STEP 5: SET UP DEQUEUE CIRCLE TO NORMAL ~ DEQUEUE FINISHED
		// -------------------------------------------------
		Timeline enQueueCircleToBlack = new Timeline();
		enQueueCircleToBlack.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getDeQueueCircle().strokeProperty(), Color.BLACK)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getDeQueueValue().textProperty(), " ")));

		// ADDING ANIMATION INTO SEQUENCE TRANSITION----------------------------------
		enQueueTransition.getChildren().add(enQueueCircleToRed);
		enQueueTransition.getChildren().add(updateFront_1);
		enQueueTransition.getChildren().add(updateFront_2);
		enQueueTransition.getChildren().add(updateFront_3);
		enQueueTransition.getChildren().add(fade);
		enQueueTransition.getChildren().add(modifyLayout);
		enQueueTransition.getChildren().add(enQueueCircleToBlack);

		enQueueTransition.setOnFinished(e -> {
			queuePer.getEnQueueButton().setDisable(false);
			queuePer.getDeQueueButton().setDisable(false);
			queuePer.getResetButton().setDisable(false);
			// when sorting complete the randomButton are set to can be activated
		});

		enQueueTransition.play();
	}

	public String getDeQueueValue() {
		return deQueueValue;
	}
}
