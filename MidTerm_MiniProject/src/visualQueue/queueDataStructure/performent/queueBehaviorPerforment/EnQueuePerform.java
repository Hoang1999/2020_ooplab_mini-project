package visualQueue.queueDataStructure.performent.queueBehaviorPerforment;

import arrowLineAnimation.ArrowHeadMoveToANewPosition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import visualQueue.queueDataStructure.performent.QueuePerform;
import visualQueue.shape.queueNode.QueueNodeManager;

public class EnQueuePerform extends QueueBehaviorPerform {
	private QueueNodeManager newNode;
	private String newNodeValue;

	public EnQueuePerform(QueuePerform queuePer) {
		super(queuePer);

	}

	public void enQueue(String newNodeValue) {
		newNode = new QueueNodeManager();
		this.newNodeValue = newNodeValue;

		queuePer.getQueue().getEnQueue().enQueue(newNodeValue);

		// change the style of the node to new node's style
		newNode.getQueueNode().getStyleClass().remove("hboxNodeNormal");
		newNode.getQueueNode().getStyleClass().add("hboxNodeNewAddedNode");

		queuePer.getQueueNodeList().put(queuePer.getQueue().getEnQueue().getNewNodeID(), newNode);

		newNode.getQueueNode().setLayoutX(100);
		newNode.getQueueNode().setLayoutY(100);

		if (queuePer.getQueueNodeList().size() == 1) {
			configVisualQueueFirstNode();
		} else
			configVisualQueue();
	}

	private void configVisualQueueFirstNode() {
		// ------------------------------------- SETTING FOR ANIMATION
		// -------------------------------------

		newNode.getArrowLineLinkCome().add(queuePer.getFront().getFrontArrow());
		newNode.getArrowLineLinkCome().add(queuePer.getRear().getRearArrow());

		// ------------------------------------- CREATE ANIMATION
		// -------------------------------------
		SequentialTransition enQueueTransition = new SequentialTransition();

		// STEP 1:
		// MOVE THE ENQUEUE CIRCLE TO GIVE VALUE TO NEW
		// NODE----------------------------------------------
		Timeline enQueueCircleToRed = new Timeline();

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getEnQueueCircle().strokeProperty(), Color.RED)),
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getEnQueueValue().textProperty(), newNodeValue)));

		double beforeX = queuePer.getEnQueueCircle().getLayoutX();
		double beforeY = queuePer.getEnQueueCircle().getLayoutY();
		double posX = queuePer.getEnQueueValue().getLayoutX() - beforeX;
		double posY = queuePer.getEnQueueValue().getLayoutY() - beforeY;

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).divide(2),
						new KeyValue(queuePer.getEnQueueCircle().layoutXProperty(),
								newNode.getQueueNode().getLayoutX() + 25, Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueCircle().layoutYProperty(),
								newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2,
								Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().layoutXProperty(), beforeX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().layoutYProperty(), beforeY)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueValue().layoutXProperty(),
								newNode.getQueueNode().getLayoutX() + 25 + posX, Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueValue().layoutYProperty(),
								newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2 + posY,
								Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueValue().layoutXProperty(), beforeX + posX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueValue().layoutYProperty(), beforeY + posY)));

		enQueueCircleToRed.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(newNode.getNodeCont().getValueText().textProperty(), newNodeValue)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getEnQueueValue().textProperty(), "")));

		enQueueCircleToRed.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().strokeProperty(), Color.BLACK)));

		// STEP 2:
		// REAR AND FRONT POINT TO THE NEW NODE AND THEN MOVE THE NEW NODE TO ITS
		// POSITION ---------------------------------------------------------
		Timeline showFrontRearArrow = new Timeline();

		showFrontRearArrow.getKeyFrames().addAll(
				new KeyFrame(new Duration(1),
						new KeyValue(queuePer.getRear().getRearArrow().getArrowLine().visibleProperty(), true)),
				new KeyFrame(new Duration(1),
						new KeyValue(queuePer.getFront().getFrontArrow().getArrowLine().visibleProperty(), true)));

		ArrowHeadMoveToANewPosition frontPointToNewNode = new ArrowHeadMoveToANewPosition(
				queuePer.getFront().getFrontArrow(), queuePer.getDuration(),
				queuePer.getFront().getFront().getLayoutX(), queuePer.getFront().getFront().getLayoutY(),
				newNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight);

		ArrowHeadMoveToANewPosition rearPointToNewNode = new ArrowHeadMoveToANewPosition(
				queuePer.getRear().getRearArrow(), queuePer.getDuration(), 250, 400,
				newNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight);

		double[][] pos = new double[newNode.getArrowLineLinkCome().size()][2];
		for (int i = 0; i < newNode.getArrowLineLinkCome().size(); i++) {
			pos[i][0] = QueueNodeManager.nodeWidth / 2;
			pos[i][1] = QueueNodeManager.nodeHeight;
		}

		MoveQueueNode newNodeMotion = new MoveQueueNode(newNode, queuePer.getDuration(), queuePer.getLeftSpace(),
				queuePer.getTopSpace(), pos);

		// ADDING ANIMATION TO SEQUENCE TRANSITION
		enQueueTransition.getChildren().add(enQueueCircleToRed);
		enQueueTransition.getChildren().add(showFrontRearArrow);
		enQueueTransition.getChildren().add(frontPointToNewNode);
		enQueueTransition.getChildren().add(rearPointToNewNode);
		enQueueTransition.getChildren().add(newNodeMotion);

		enQueueTransition.setOnFinished(e -> {
			queuePer.getEnQueueButton().setDisable(false);
			queuePer.getDeQueueButton().setDisable(false);
			queuePer.getResetButton().setDisable(false);
			// when sorting complete the randomButton are set to can be activated
		});

		enQueueTransition.play();
	}

	private void configVisualQueue() {
		// ------------------------------------- SETTING FOR ANIMATION
		// -------------------------------------
		QueueNodeManager almostLastNode = queuePer.getQueueNodeList().get(queuePer.getQueue().getRear() - 1);

		// set the style of the almost last node
		almostLastNode.getQueueNode().getStyleClass().remove("hboxNodeNewAddedNode");
		almostLastNode.getQueueNode().getStyleClass().add("hboxNodeNormal");

		// ------------------------------------- CREATE ANIMATION
		// -------------------------------------
		SequentialTransition enQueueTransition = new SequentialTransition();

		// STEP 1:
		// MOVE THE ENQUEUE CIRCLE TO GIVE VALUE TO NEW
		// NODE----------------------------------------------
		Timeline enQueueCircleToRed = new Timeline();
		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getEnQueueCircle().strokeProperty(), Color.RED)),
				new KeyFrame(new Duration(1), new KeyValue(queuePer.getEnQueueValue().textProperty(), newNodeValue)));

		double beforeX = queuePer.getEnQueueCircle().getLayoutX();
		double beforeY = queuePer.getEnQueueCircle().getLayoutY();
		double posX = queuePer.getEnQueueValue().getLayoutX() - beforeX;
		double posY = queuePer.getEnQueueValue().getLayoutY() - beforeY;

		enQueueCircleToRed.getKeyFrames().addAll(
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueCircle().layoutXProperty(),
								newNode.getQueueNode().getLayoutX() + 25, Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueCircle().layoutYProperty(),
								newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2,
								Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().layoutXProperty(), beforeX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().layoutYProperty(), beforeY)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueValue().layoutXProperty(),
								newNode.getQueueNode().getLayoutX() + 25 + posX, Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(queuePer.getEnQueueValue().layoutYProperty(),
								newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2 + posY,
								Interpolator.EASE_BOTH)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueValue().layoutXProperty(), beforeX + posX)),
				new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueValue().layoutYProperty(), beforeY + posY)));

		enQueueCircleToRed.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()).multiply(2),
						new KeyValue(queuePer.getEnQueueCircle().strokeProperty(), Color.BLACK)));

		enQueueCircleToRed.getKeyFrames()
				.addAll(new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
						new KeyValue(newNode.getNodeCont().getValueText().textProperty(), newNodeValue)),
						new KeyFrame(new Duration(queuePer.getDuration().toMillis()),
								new KeyValue(queuePer.getEnQueueValue().textProperty(), "")));

		// STEP 2:
		// MAKE LINK TO NEW NODE AND PULL IT TO ITS NEW POSITION
		// ---------------------------------------
		new MakeArrowLink(queuePer).create(almostLastNode, newNode);

		ArrowHeadMoveToANewPosition arrowLinkToNewNode = new ArrowHeadMoveToANewPosition(
				almostLastNode.getArrowLineLinkTo(), queuePer.getDuration(),
				almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth,
				almostLastNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2,
				newNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				newNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight);

		double[][] pos = new double[newNode.getArrowLineLinkCome().size()][2];
		for (int i = 0; i < newNode.getArrowLineLinkCome().size(); i++) {
			pos[i][0] = newNode.getArrowLineLinkCome().get(i).getHeadLayoutX() - newNode.getQueueNode().getLayoutX();
			pos[i][1] = newNode.getArrowLineLinkCome().get(i).getHeadLayoutY() - newNode.getQueueNode().getLayoutY();
		}

		MoveQueueNode newNodeMotion = new MoveQueueNode(newNode, queuePer.getDuration(),
				almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth
						+ queuePer.getSpaceBetweenNode(),
				queuePer.getTopSpace(), pos);

		// STEP 3:
		// UPDATE THE REAR NODE
		// ------------------------------------------------------------------------------------
		almostLastNode.getArrowLineLinkCome().remove(queuePer.getRear().getRearArrow());

		double updateRear_1_headDesX = almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth - 13;
		double updateRear_1_headDesY = queuePer.getTopSpace() + QueueNodeManager.nodeHeight / 2;

		ArrowHeadMoveToANewPosition updateRear_1 = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				queuePer.getDuration().divide(4),
				almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth / 2,
				queuePer.getTopSpace() + QueueNodeManager.nodeHeight, updateRear_1_headDesX, updateRear_1_headDesY);

		double updateRear_2_headDesX = almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth
				+ queuePer.getSpaceBetweenNode();
		double updateRear_2_headDesY = almostLastNode.getQueueNode().getLayoutY() + QueueNodeManager.nodeHeight / 2;

		ArrowHeadMoveToANewPosition updateRear_2 = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				queuePer.getDuration().divide(4), updateRear_1_headDesX, updateRear_1_headDesY, updateRear_2_headDesX,
				updateRear_2_headDesY);

		double updateRear_3_headDesX = almostLastNode.getQueueNode().getLayoutX() + QueueNodeManager.nodeWidth
				+ queuePer.getSpaceBetweenNode() + QueueNodeManager.nodeWidth / 2;
		double updateRear_3_headDesY = queuePer.getTopSpace() + QueueNodeManager.nodeHeight;

		ArrowHeadMoveToANewPosition updateRear_3 = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				queuePer.getDuration().divide(4), updateRear_2_headDesX, updateRear_2_headDesY, updateRear_3_headDesX,
				updateRear_3_headDesY);

		// ADD TO SEQUENCE ANIMATION
		// -----------------------------------------------------------------------------
		enQueueTransition.getChildren().add(enQueueCircleToRed);
		enQueueTransition.getChildren().add(arrowLinkToNewNode);
		enQueueTransition.getChildren().add(newNodeMotion);
		enQueueTransition.getChildren().add(updateRear_1);
		enQueueTransition.getChildren().add(updateRear_2);
		enQueueTransition.getChildren().add(updateRear_3);

		enQueueTransition.setOnFinished(e -> {
			queuePer.getEnQueueButton().setDisable(false);
			queuePer.getDeQueueButton().setDisable(false);
			queuePer.getResetButton().setDisable(false);
		});

		enQueueTransition.play();
	}

	public QueueNodeManager getNewNode() {
		return newNode;
	}
}
