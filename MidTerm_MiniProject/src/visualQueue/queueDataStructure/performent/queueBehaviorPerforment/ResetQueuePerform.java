package visualQueue.queueDataStructure.performent.queueBehaviorPerforment;

import arrowLineAnimation.ArrowHeadMoveToANewPosition;
import javafx.util.Duration;
import visualQueue.queueDataStructure.performent.QueuePerform;

public class ResetQueuePerform extends QueueBehaviorPerform {

	public ResetQueuePerform(QueuePerform queuePer) {
		super(queuePer);
	}

	public void reset() {
		queuePer.getQueue().getResetQueue().reset();

		queuePer.getRear().getRearArrow().getArrowLine().setVisible(false);
		queuePer.getFront().getFrontArrow().getArrowLine().setVisible(false);

		ArrowHeadMoveToANewPosition frontPointDel = new ArrowHeadMoveToANewPosition(queuePer.getFront().getFrontArrow(),
				new Duration(1), 0.0, 0.0, queuePer.getFront().getFrontArrow().getFootLayoutX(),
				queuePer.getFront().getFrontArrow().getFootLayoutY());
		frontPointDel.play();

		ArrowHeadMoveToANewPosition rearPointDel = new ArrowHeadMoveToANewPosition(queuePer.getRear().getRearArrow(),
				new Duration(1), 0.0, 0.0, queuePer.getRear().getRearArrow().getFootLayoutX(),
				queuePer.getRear().getRearArrow().getFootLayoutY());
		rearPointDel.play();

		rearPointDel.setOnFinished(e -> {
			queuePer.getEnQueueButton().setDisable(false);
			queuePer.getDeQueueButton().setDisable(false);
			queuePer.getResetButton().setDisable(false);
			// when sorting complete the randomButton are set to can be activated
		});
	}

}
