package visualQueue.queueDataStructure.performent;

import arrowLineManager.ArrowLineManager;
import javafx.scene.control.Button;

public class Rear {
	private final Button rear;

	private final ArrowLineManager rearArrow;

	public Rear() {
		rear = new Button("REAR");
		rearArrow = new ArrowLineManager();
		rearArrow.setStrokeWidth(1.5);
		rearArrow.setArrowScale(0.15, 0.15);
	}

	public Button getRear() {
		return rear;
	}

	public ArrowLineManager getRearArrow() {
		return rearArrow;
	}
}
