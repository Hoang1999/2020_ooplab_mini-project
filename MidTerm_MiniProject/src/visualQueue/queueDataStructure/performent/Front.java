package visualQueue.queueDataStructure.performent;

import arrowLineManager.ArrowLineManager;
import javafx.scene.control.Button;

public class Front {
	private final Button front;

	private final ArrowLineManager frontArrow;

	public Front() {
		front = new Button("FRONT");

		frontArrow = new ArrowLineManager();
		frontArrow.setStrokeWidth(1.5);
		frontArrow.setArrowScale(0.15, 0.15);
	}

	public Button getFront() {
		return front;
	}

	public ArrowLineManager getFrontArrow() {
		return frontArrow;
	}
}
