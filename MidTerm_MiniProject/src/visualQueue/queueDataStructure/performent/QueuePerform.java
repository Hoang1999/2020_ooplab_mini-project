package visualQueue.queueDataStructure.performent;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.scene.control.Button;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import visualQueue.queueDataStructure.code.Queue;
import visualQueue.queueDataStructure.performent.queueBehaviorPerforment.DeQueuePerform;
import visualQueue.queueDataStructure.performent.queueBehaviorPerforment.EnQueuePerform;
import visualQueue.queueDataStructure.performent.queueBehaviorPerforment.ResetQueuePerform;
import visualQueue.shape.queueNode.QueueNodeManager;

public class QueuePerform {
	private double topSpace = 200;
	private double leftSpace = 100;
	private double spaceBetweenNode = 30;

	private Duration duration = new Duration(500);

	private final Queue queue = new Queue();

	private HashMap<Integer, QueueNodeManager> queueNodeList = new HashMap<Integer, QueueNodeManager>();
	private ArrayList<Integer> removeQueueNodeList = new ArrayList<Integer>();

	private final Front front;
	private final Rear rear;

	private Circle enQueueCircle;
	private Circle deQueueCircle;
	private Text enQueueValue;
	private Text deQueueValue;
	private Button EnQueueButton;
	private Button DeQueueButton;
	private Button ResetButton;

	private final DeQueuePerform deQueuePer = new DeQueuePerform(this);
	private final EnQueuePerform enQueuePer = new EnQueuePerform(this);
	private final ResetQueuePerform reset = new ResetQueuePerform(this);

	public QueuePerform() {
		front = new Front();
		front.getFront().setId("front-button");
		front.getFront().getStylesheets().add(getClass().getResource("./css/performent.css").toExternalForm());

		rear = new Rear();
		rear.getRear().setId("rear-button");
		rear.getRear().getStylesheets().add(getClass().getResource("./css/performent.css").toExternalForm());
	}

	public void getNode(Circle enQueueCircle, Text enQueueValue, Circle deQueueCircle, Text deQueueValue,
			Button EnQueueButton, Button DeQueueButton, Button ResetButton) {
		this.enQueueCircle = enQueueCircle;
		this.enQueueValue = enQueueValue;
		this.deQueueCircle = deQueueCircle;
		this.deQueueValue = deQueueValue;
		this.EnQueueButton = EnQueueButton;
		this.DeQueueButton = DeQueueButton;
		this.ResetButton = ResetButton;
	}

	public HashMap<Integer, QueueNodeManager> getQueueNodeList() {
		return queueNodeList;
	}

	public Queue getQueue() {
		return queue;
	}

	public double getTopSpace() {
		return topSpace;
	}

	public void setTopSpace(double topSpace) {
		this.topSpace = topSpace;
	}

	public double getLeftSpace() {
		return leftSpace;
	}

	public void setLeftSpace(double leftSpace) {
		this.leftSpace = leftSpace;
	}

	public double getSpaceBetweenNode() {
		return spaceBetweenNode;
	}

	public void setSpaceBetweenNode(double spaceBetweenNode) {
		this.spaceBetweenNode = spaceBetweenNode;
	}

	public DeQueuePerform getDeQueuePer() {
		return deQueuePer;
	}

	public EnQueuePerform getEnQueuePer() {
		return enQueuePer;
	}

	public ResetQueuePerform getReset() {
		return reset;
	}

	public Front getFront() {
		return front;
	}

	public Rear getRear() {
		return rear;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public Circle getEnQueueCircle() {
		return enQueueCircle;
	}

	public void setEnQueueCircle(Circle enQueueCircle) {
		this.enQueueCircle = enQueueCircle;
	}

	public Circle getDeQueueCircle() {
		return deQueueCircle;
	}

	public void setDeQueueCircle(Circle deQueueCircle) {
		this.deQueueCircle = deQueueCircle;
	}

	public Text getEnQueueValue() {
		return enQueueValue;
	}

	public void setEnQueueValue(Text enQueueValue) {
		this.enQueueValue = enQueueValue;
	}

	public Text getDeQueueValue() {
		return deQueueValue;
	}

	public void setDeQueueValue(Text deQueueValue) {
		this.deQueueValue = deQueueValue;
	}

	public ArrayList<Integer> getRemoveQueueNodeList() {
		return removeQueueNodeList;
	}

	public Button getEnQueueButton() {
		return EnQueueButton;
	}

	public Button getDeQueueButton() {
		return DeQueueButton;
	}

	public Button getResetButton() {
		return ResetButton;
	}
}
