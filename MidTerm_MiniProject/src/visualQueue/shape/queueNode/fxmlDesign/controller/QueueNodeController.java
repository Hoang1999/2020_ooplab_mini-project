package visualQueue.shape.queueNode.fxmlDesign.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class QueueNodeController implements Initializable {
	@FXML
	private Text valueText;
	@FXML
	private Circle linkFrom;
	@FXML
	private Circle linkTo;
	@FXML
	private Line strike1;
	@FXML
	private Line strike2;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public Circle getLinkFrom() {
		return linkFrom;
	}

	public Circle getLinkTo() {
		return linkTo;
	}

	public Line getStrike1() {
		return strike1;
	}

	public Line getStrike2() {
		return strike2;
	}

	public Text getValueText() {
		return valueText;
	}

	public void setValueText(Text valueText) {
		this.valueText = valueText;
	}
}
