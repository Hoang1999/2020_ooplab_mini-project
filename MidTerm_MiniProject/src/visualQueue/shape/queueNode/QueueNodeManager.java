package visualQueue.shape.queueNode;

import java.util.ArrayList;

import arrowLineManager.ArrowLineManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import visualQueue.shape.queueNode.fxmlDesign.controller.QueueNodeController;

public class QueueNodeManager {
	private HBox queueNode = null;
	private String queueNodeFXML = "./fxmlDesign/QueueNode.fxml";

	// NOTICE: suppose that every queue node have the same size
	public static double nodeWidth;
	public static double nodeHeight;

	private ArrowLineManager arrowLineLinkTo;
	private ArrayList<ArrowLineManager> arrowLineLinkCome;

	private final QueueNodeController nodeCont;

	public QueueNodeManager() {
		nodeCont = new QueueNodeController();
		FXMLLoader hboxNodeLoader = new FXMLLoader(getClass().getResource(queueNodeFXML));
		hboxNodeLoader.setController(nodeCont);
		try {
			queueNode = hboxNodeLoader.load();
			arrowLineLinkCome = new ArrayList<ArrowLineManager>();

			nodeWidth = queueNode.getPrefWidth();
			nodeHeight = queueNode.getPrefHeight();
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.initializeQueueNode();
	}

	private void initializeQueueNode() {
		queueNode.getStylesheets().add(getClass().getResource("./cssDesign/queueNode.css").toExternalForm());
	}

	public HBox getQueueNode() {
		return queueNode;
	}

	public void setQueueNode(HBox queueNode) {
		this.queueNode = queueNode;
	}

	public ArrowLineManager getArrowLineLinkTo() {
		return arrowLineLinkTo;
	}

	public void setArrowLineLinkTo(ArrowLineManager arrowLineLinkTo) {
		this.arrowLineLinkTo = arrowLineLinkTo;
	}

	public ArrayList<ArrowLineManager> getArrowLineLinkCome() {
		return arrowLineLinkCome;
	}

	public void setArrowLineLinkCome(ArrayList<ArrowLineManager> arrowLineLinkCome) {
		this.arrowLineLinkCome = arrowLineLinkCome;
	}

	public QueueNodeController getNodeCont() {
		return nodeCont;
	}
}
