package visualQueue;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import visualQueue.queueDataStructure.performent.QueuePerform;
import visualQueue.shape.queueNode.QueueNodeManager;

public class RootController implements Initializable {

	@FXML
	private AnchorPane performPane;
	@FXML
	private TextField newNodeName;
	@FXML
	private Circle enQueueCircle;
	@FXML
	private Circle deQueueCircle;
	@FXML
	private Text enQueueValue;
	@FXML
	private Text deQueueValue;
	@FXML
	private Button enQueueButton;
	@FXML
	private Button deQueueButton;
	@FXML
	private Button resetButton;
	@FXML
	private Slider animationSpeed;

	private QueuePerform queuePer = new QueuePerform();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		enQueueCircle.setViewOrder(-1);
		deQueueCircle.setViewOrder(-1);
		enQueueValue.setViewOrder(-1);
		deQueueValue.setViewOrder(-1);

		// Set up for front button and front arrow
		// ==============================================================
		performPane.getChildren().add(queuePer.getFront().getFront());
		queuePer.getFront().getFront().setLayoutX(150);
		queuePer.getFront().getFront().setLayoutY(400);
		queuePer.getFront().getFront().setViewOrder(-1);

		performPane.getChildren().add(queuePer.getFront().getFrontArrow().getArrowLine());
		queuePer.getFront().getFrontArrow().setPosition(queuePer.getFront().getFront().getLayoutX() + 30,
				queuePer.getFront().getFront().getLayoutY() + 30, 100.0, 200.0);
		queuePer.getFront().getFrontArrow().getArrowLine().setVisible(false);
		queuePer.getFront().getFrontArrow().getArrowLine().setViewOrder(0);
		// =======================================================================================================

		// Set up for rear button and rear arrow
		// ==============================================================
		performPane.getChildren().add(queuePer.getRear().getRear());
		queuePer.getRear().getRear().setLayoutX(250);
		queuePer.getRear().getRear().setLayoutY(400);
		queuePer.getRear().getRear().setViewOrder(-1);

		performPane.getChildren().add(queuePer.getRear().getRearArrow().getArrowLine());
		queuePer.getRear().getRearArrow().setPosition(queuePer.getRear().getRear().getLayoutX() + 30,
				queuePer.getRear().getRear().getLayoutY() + 30, 200, 200);
		queuePer.getRear().getRearArrow().getArrowLine().setVisible(false);
		queuePer.getRear().getRearArrow().getArrowLine().setViewOrder(0);
		// =======================================================================================================

		queuePer.getNode(enQueueCircle, enQueueValue, deQueueCircle, deQueueValue, enQueueButton, deQueueButton,
				resetButton);

		// Set up for animation speed function
		// Adding Listener to value property.
		animationSpeed.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				queuePer.setDuration(new Duration(-new_val.doubleValue()));
			}
		});
	}

	public void enQueue(ActionEvent event) {
		System.out.println("EnQueue");

		// Delete node marked at removeQueueNode
		if (queuePer.getRemoveQueueNodeList().size() > 0)
			for (int i = queuePer.getRemoveQueueNodeList().size() - 1; i >= 0; i--) {
				int index = queuePer.getRemoveQueueNodeList().get(i);
				queuePer.getRemoveQueueNodeList().remove(i);

				QueueNodeManager delNode = queuePer.getQueueNodeList().get(index);
				performPane.getChildren().remove(delNode.getQueueNode());

				if (delNode.getArrowLineLinkTo() != null) {
					performPane.getChildren().remove(delNode.getArrowLineLinkTo().getArrowLine());
				}
				queuePer.getQueueNodeList().remove(index);
			}

		// start enqueue
		if (newNodeName.getText().contentEquals("")) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Empty input");
			alert.setHeaderText("Please enter a name for queue node!");
			alert.setContentText("A queue node need a name before be enqueued");
			alert.showAndWait();
			return;
		}

		enQueueButton.setDisable(true);
		deQueueButton.setDisable(true);
		resetButton.setDisable(true);

		queuePer.getEnQueuePer().enQueue(newNodeName.getText());
		newNodeName.clear();

		QueueNodeManager newNodex = queuePer.getEnQueuePer().getNewNode();
		performPane.getChildren().add(newNodex.getQueueNode());

		if (queuePer.getQueueNodeList().size() > 1)
			performPane.getChildren().add(queuePer.getQueueNodeList().get(queuePer.getQueue().getRear() - 1)
					.getArrowLineLinkTo().getArrowLine());
	}

	public void deQueue(ActionEvent event) {
		System.out.println("DeQueue");

		if (queuePer.getQueue().getFront() <= 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Empty queue");
			alert.setHeaderText("Queue is empty now!");
			alert.setContentText("There are nothing to be deleted");
			alert.showAndWait();
			return;
		}

		enQueueButton.setDisable(true);
		deQueueButton.setDisable(true);
		resetButton.setDisable(true);

		queuePer.getDeQueuePer().deQueue();
	}

	private Label label = new Label();

	public void reset(ActionEvent event) {
		System.out.println("Reset");

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Reset Queue");
		alert.setHeaderText("Are you sure want to reset this queue?");
		alert.setContentText("This act cannot be undone");

		// option != null.
		Optional<ButtonType> option = alert.showAndWait();

		if (option.get() == null) {
			this.label.setText("No selection!");
		} else if (option.get() == ButtonType.OK) {
			this.label.setText("File deleted!");

			enQueueButton.setDisable(true);
			deQueueButton.setDisable(true);
			resetButton.setDisable(true);

			queuePer.getReset().reset();

			queuePer.getQueueNodeList().forEach((k, v) -> {
				performPane.getChildren().remove(v.getQueueNode());
				if (v.getArrowLineLinkTo() != null) {
					performPane.getChildren().remove(v.getArrowLineLinkTo().getArrowLine());
				}
			});

			queuePer.getQueueNodeList().clear();
			queuePer.getRemoveQueueNodeList().clear();

		} else if (option.get() == ButtonType.CANCEL) {
			this.label.setText("Cancelled!");
		} else {
			this.label.setText("-");
		}
	}
}