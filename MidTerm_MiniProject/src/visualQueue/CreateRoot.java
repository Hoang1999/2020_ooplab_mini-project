package visualQueue;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class CreateRoot {
	private Parent root = null;

	private String rootFXML = "CreateRoot.fxml";
	FXMLLoader rootLoader;
	private RootController rootCont;

	public CreateRoot() {
		rootLoader = new FXMLLoader(getClass().getResource(rootFXML));
		rootCont = new RootController();
		rootLoader.setController(rootCont);
		try {
			setRoot(rootLoader.load());
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.addCSSFile();
	}

	public void addCSSFile() {
		root.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.getStylesheets().add(getClass().getResource("./cssStyleSheets/buttonStyle.css").toExternalForm());
	}

	public Parent getRoot() {
		return root;
	}

	public void setRoot(Parent root) {
		this.root = root;
	}
}
