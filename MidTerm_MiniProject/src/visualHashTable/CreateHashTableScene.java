package visualHashTable;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class CreateHashTableScene {
	public Scene createScene() throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("./JavaFX.fxml"));
		JavaFXController menuCont = new JavaFXController();
		loader.setController(menuCont);

		Parent root = loader.load();

		Scene scene = new Scene(root);

		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		return scene;
	}
}
