package visualHashTable;

import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class JavaFXController implements Initializable {

	@FXML
	private AnchorPane paneHashTable;
	@FXML
	private AnchorPane paneText;

	@FXML
	private Button InsertB;
	@FXML
	private Button DeleteB;
	@FXML
	private Button FindB;
	@FXML
	private Button ResetB;

	@FXML
	private TextField InsertTF;
	@FXML
	private TextField DeleteTF;
	@FXML
	private TextField FindTF;

	@FXML
	private HBox hBox;

	@FXML
	private Text text;

	@FXML
	private Slider animationSpeed;

	public double DURATION = 1000;
	private HashingFX hashing = new HashingFX(false);

	private Hashtable<Integer, HashingKey> hashTableKey = new Hashtable<Integer, HashingKey>();

	//
	@FXML
	public void InsertFX(ActionEvent event) {
		String str = InsertTF.getText();
		if (!hashing.isExists())
			setNew();
		hashing.Insert(str, paneHashTable, text, DURATION);
		InsertTF.clear();
	}

	//
	@FXML
	public void DeleteFX(ActionEvent event) {
		String str = DeleteTF.getText();
		hashing.Delete(str, paneHashTable, text, DURATION);
		DeleteTF.clear();
	}

	//
	@FXML
	public void FindFX(ActionEvent event) {
		String str = FindTF.getText();
		hashing.Find(str, text, DURATION);
		FindTF.clear();
	}

	//
	@FXML
	public void ResetFX(ActionEvent event) {
		hashing.Reset();
		paneHashTable.getChildren().clear();
		hBox.getChildren().clear();
		text.setText("");

	}

	//
	public void setNew() {
		animationSpeed.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				DURATION = -new_val.doubleValue();
			}
		});
		hashing = new HashingFX(true);
		for (int i = 0; i < new HashingValue().MOD_NUMBER; i++) {
			hashTableKey.put(i, new HashingKey(i));
		}
		int key;
		Enumeration<Integer> keys = hashTableKey.keys();
		while (keys.hasMoreElements()) {
			key = (int) keys.nextElement();
			hBox.getChildren().add(hashTableKey.get(key).getKeyNode());
		}
	}

	//
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	//
}
