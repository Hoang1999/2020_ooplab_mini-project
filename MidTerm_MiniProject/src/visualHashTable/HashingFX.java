package visualHashTable;

import java.util.ArrayList;
import java.util.Hashtable;

import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

public class HashingFX extends HashingValue {

	private Hashtable<Integer, ArrayList<HashingValue>> hashTable = new Hashtable<Integer, ArrayList<HashingValue>>();
	private HashingValue hashingValue = new HashingValue();
	private boolean exists = false;
	private int key;
	private String textOut;

	//
	public HashingFX(boolean status) {
		if (status == true) {
			for (int i = 0; i < hashingValue.MOD_NUMBER; i++)
				hashTable.put(i, new ArrayList<HashingValue>());
		}
		this.exists = status;
	}

	//
	public void Insert(String str, AnchorPane paneHashTable, Text text, double duration) {
		hashingValue = new HashingValue(str);
		key = hashingValue.hashCode();
		textOut = new String();

		textOut = "Inserting";
		if (str.length() == 0) {
			textOut += " | " + "No string have been read!";
			print(text, textOut);
			return;
		}
		textOut += " | " + "Key's Code = " + hashingValue.getTextHashCode();
		for (int i = 0; i < hashTable.get(key).size(); i++) {
			if (str.equals(hashTable.get(key).get(i).getValue())) {
				textOut += " | " + "Already have!";
				print(text, textOut);
				return;
			}
		}
		if (hashTable.get(key).size() - 1 >= 0)
			hashTable.get(key).get(hashTable.get(key).size() - 1).hasValue();
		hashingValue = new HashingValue(str, key, hashTable.get(key).size());
		hashTable.get(key).add(hashingValue);
		hashingValue.specialMoveValue(
				new HashingKey().CIRCLE_POSITION_Y - 100 - hashingValue.HEIGHT_VALUE * 2 * hashTable.get(key).size(),
				duration);
		paneHashTable.getChildren().addAll(hashingValue.getValueNode());

		textOut += " | " + "Insert " + str + " successly!";
		print(text, textOut);
	}

	//
	public void Delete(String str, AnchorPane paneHashTable, Text text, double duration) {
		hashingValue = new HashingValue(str);
		key = hashingValue.hashCode();
		textOut = new String();

		textOut = "Deleting";
		if (this.exists == false) {
			textOut += " | " + "Haven't have anything yet!";
			print(text, textOut);
			return;
		}
		textOut += " | " + "Key's Code = " + hashingValue.getTextHashCode();
		for (int i = 0; i < hashTable.get(key).size(); i++) {
			if (str.equals(hashTable.get(key).get(i).getValue())) {
				paneHashTable.getChildren().remove(hashTable.get(key).get(i).getValueNode());
				hashTable.get(key).remove(i);
				if (i == hashTable.get(key).size() && hashTable.get(key).size() > 0)
					hashTable.get(key).get(hashTable.get(key).size() - 1).noValue();
				for (int j = i; j < hashTable.get(key).size(); j++) {
					hashTable.get(key).get(j).moveValueNodeDown(duration);
				}
				textOut += " | " + "Delete successly!";
				print(text, textOut);
				return;
			}
		}

		textOut += " | " + "No string likes that in table!";
		print(text, textOut);
	}

	//
	public void Find(String str, Text text, double duration) {
		hashingValue = new HashingValue(str);
		key = hashingValue.hashCode();
		textOut = new String();

		textOut += "Finding";
		if (this.exists == false) {
			textOut += " | " + "Haven't have anything yet!";
			print(text, textOut);
			return;
		}
		textOut += " | " + "Key's Code = " + hashingValue.getTextHashCode();
		for (int i = 0; i < hashTable.get(key).size(); i++) {
			if (str.equals(hashTable.get(key).get(i).getValue())) {
				hashTable.get(key).get(i).foundLightRotate(duration);
				textOut += " | " + "String have been found!";
				print(text, textOut);
				return;
			}
		}
		textOut += " | " + "No string likes that in table!";
		print(text, textOut);
	}

	//
	public void Reset() {
		hashTable.clear();
		this.exists = false;
	}

	//
	public void print(Text text, String str) {
		text.setText(str);
	}

	//
	public Hashtable<Integer, ArrayList<HashingValue>> getHashTable() {
		return hashTable;
	}

	public void setHashTable(Hashtable<Integer, ArrayList<HashingValue>> hashTable) {
		this.hashTable = hashTable;
	}

	public boolean isExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}
}
