package visualHashTable;

import arrowLineAnimation.ArrowFootMoveToANewPosition;
import arrowLineManager.ArrowLineManager;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.StrokeTransition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class HashingValue {

	private Pane valueNode = new Pane();
	private Rectangle rect = new Rectangle();
	private VBox vBox = new VBox();
	private Line lineNode1 = new Line();
	private Line lineNode2 = new Line();
	private Text textNode = new Text();
	private String value = new String();
	private Pane pane = new Pane();
	private StackPane stackPane = new StackPane();
	private int positionX;
	private int positionY;
	private ArrowLineManager arrow = new ArrowLineManager();
	private String textHashCode;

	public final int MOD_NUMBER = 11;
	public final int WIDTH_VALUE = 80;
	public final int HEIGHT_VALUE = 50;

	public HashingValue() {

	}

	public HashingValue(String value) {
		this.value = value;
	}

	public HashingValue(String value, int positionX, int positionY) {
		this.value = value;
		this.positionX = positionX;
		this.positionY = positionY;

		rect.setHeight(HEIGHT_VALUE);
		rect.setWidth(WIDTH_VALUE);
		rect.setFill(Color.WHITE);
		rect.setStroke(Color.BLACK);
		rect.setStrokeWidth(2);

		vBox.setStyle("-fx-border-style: solid;");

		lineNode1.setStartX(WIDTH_VALUE);
		lineNode1.setEndY(HEIGHT_VALUE / 5);

		lineNode2.setStartY(HEIGHT_VALUE / 5);
		lineNode2.setEndX(WIDTH_VALUE);
		lineNode2.setEndY(HEIGHT_VALUE / 5);

		pane.setPrefSize(WIDTH_VALUE, HEIGHT_VALUE / 5);
		pane.getChildren().addAll(lineNode1, lineNode2);

		textNode.setFont(Font.font("Abyssinica SIL", FontWeight.BOLD, FontPosture.REGULAR, 20));
		textNode.setTextAlignment(TextAlignment.CENTER);
		textNode.setText(value);

		arrow.setPosition(
				new HashingKey().FIRST_CIRCLE_POSITION_X + (MOD_NUMBER - 1) * new HashingKey().WIDTH_KEY
						- new HashingKey().WIDTH_KEY * positionX,
				new HashingKey().CIRCLE_POSITION_Y - new HashingKey().WIDTH_KEY
						- new HashingKey().WIDTH_KEY * positionY,
				WIDTH_VALUE / 2, HEIGHT_VALUE);
		arrow.setArrowScale(0.1, 0.1);
		arrow.setStrokeWidth(1);

		stackPane.setPrefSize(WIDTH_VALUE, HEIGHT_VALUE * 4 / 5);
		stackPane.getChildren().addAll(textNode);
		stackPane.setAlignment(Pos.CENTER);

		vBox.getChildren().addAll(pane, stackPane);
		valueNode.getChildren().addAll(rect, vBox, arrow.getArrowLine());
	}

	//
	public int hashCode() {
		char c;
		int sum = 0;
		this.textHashCode = new String();
		textHashCode += "(";

		for (int i = 0; i < this.value.length(); i++) {
			c = this.value.charAt(i);
			if (i < this.value.length() - 1)
				textHashCode += (int) c + " + ";
			else
				textHashCode += (int) c;
			sum += c;
		}
		textHashCode += ") % " + MOD_NUMBER + " = " + sum % MOD_NUMBER;
		return sum % MOD_NUMBER;
	}

	//
	public void moveValueNodeDown(double duration) {
		TranslateTransition translate = new TranslateTransition();
		translate.setInterpolator(Interpolator.LINEAR);
		translate.setByY(100);
		translate.setDuration(Duration.millis(duration));
		translate.setNode(this.valueNode);
		translate.play();
	}

	//
	public void specialMoveValue(double y, double duration) {
		ParallelTransition transition = new ParallelTransition();

		TranslateTransition translate = new TranslateTransition();
		translate.setInterpolator(Interpolator.LINEAR);
		translate.setToX((new HashingKey().WIDTH_KEY - WIDTH_VALUE) / 2 + new HashingKey().FIRST_CIRCLE_POSITION_X
				- (new HashingKey().WIDTH_KEY / 2) + (MOD_NUMBER - 1) * new HashingKey().WIDTH_KEY
				- new HashingKey().WIDTH_KEY * positionX);
		translate.setToY(y);
		translate.setDuration(Duration.millis(duration));
		ArrowFootMoveToANewPosition moveArrow = new ArrowFootMoveToANewPosition(arrow, Duration.millis(duration),
				new HashingKey().FIRST_CIRCLE_POSITION_X + (MOD_NUMBER - 1) * new HashingKey().WIDTH_KEY
						- new HashingKey().WIDTH_KEY * positionX,
				new HashingKey().CIRCLE_POSITION_Y - new HashingKey().WIDTH_KEY
						- new HashingKey().WIDTH_KEY * positionY,
				WIDTH_VALUE / 2, HEIGHT_VALUE * 2);
		translate.setNode(this.valueNode);
		transition.getChildren().addAll(moveArrow, translate);
		transition.play();
	}

	//
	public void foundLightRotate(double duration) {
		ParallelTransition transition = new ParallelTransition();

		StrokeTransition stroke = new StrokeTransition();
		stroke.setDuration(Duration.millis(duration * 2));
		stroke.setFromValue(Color.RED);
		stroke.setToValue(Color.BLACK);
		stroke.setShape(rect);

		transition.getChildren().addAll(stroke);
		transition.play();
	}

	//
	public Pane getValueNode() {
		return valueNode;
	}

	public void setValueNode(VBox valueNode) {
		this.valueNode = valueNode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void noValue() {
		this.lineNode1.setVisible(true);
	}

	public void hasValue() {
		this.lineNode1.setVisible(false);
	}

	public String getTextHashCode() {
		return textHashCode;
	}

	public void setTextHashCode(String textHashCode) {
		this.textHashCode = textHashCode;
	}
}
