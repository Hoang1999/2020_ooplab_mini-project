package visualHashTable;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class HashingKey {

	private int key;
	private VBox keyNode = new VBox();
	private Circle circleNode = new Circle();
	private Text textNode = new Text();

	public final int WIDTH_KEY = 100;
	public final int HEIGHT_KEY = 60;
	public final int CIRCLE_RADIUS = 5;
	public final int FIRST_CIRCLE_POSITION_X = 100;
	public final int CIRCLE_POSITION_Y = 620;

	//
	public HashingKey() {
	}

	public HashingKey(int position) {

		this.key = position;
		this.keyNode.setPrefHeight(HEIGHT_KEY);
		this.keyNode.setPrefWidth(WIDTH_KEY);

		this.circleNode.setRadius(CIRCLE_RADIUS);
		this.circleNode.setFill(Color.BLACK);

		this.textNode.setTextAlignment(TextAlignment.CENTER);
		this.textNode.setText("" + this.key);

		this.keyNode.getChildren().addAll(circleNode, textNode);
		this.keyNode.setAlignment(Pos.CENTER);
	}

	//
	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public VBox getKeyNode() {
		return keyNode;
	}

	public void setKeyNode(VBox keyNode) {
		this.keyNode = keyNode;
	}

}
