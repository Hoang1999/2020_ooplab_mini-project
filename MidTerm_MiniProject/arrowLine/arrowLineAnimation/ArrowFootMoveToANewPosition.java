package arrowLineAnimation;

import java.util.HashMap;

import arrowLineManager.ArrowLineManager;
import arrowLineManager.ArrowLinePositionPropertyCaculate;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.util.Duration;

public class ArrowFootMoveToANewPosition extends Transition {
	private final ArrowLineManager arrowLine;

	private final double footDestinationX;
	private final double footDestinationY;

	private final double startFootLayoutX;
	private final double startFootLayoutY;

	public ArrowFootMoveToANewPosition(ArrowLineManager arrowLine, Duration duration, double startFootLayoutX,
			double startFootLayoutY, double footDestinationX, double footDestinationY) {
		this.arrowLine = arrowLine;

		this.footDestinationX = footDestinationX;
		this.footDestinationY = footDestinationY;

		this.startFootLayoutX = startFootLayoutX;
		this.startFootLayoutY = startFootLayoutY;

		setCycleDuration(duration);
		setInterpolator(Interpolator.LINEAR);
	}

	@Override
	protected void interpolate(double frec) {
		final double translationDegreeX = footDestinationX - startFootLayoutX;
		final double translationDegreeY = footDestinationY - startFootLayoutY;

		double currentFootLayoutX = startFootLayoutX + translationDegreeX * frec;
		double currentFootLayoutY = startFootLayoutY + translationDegreeY * frec;

		arrowLine.getArrowLine().setLayoutX(currentFootLayoutX);
		arrowLine.getArrowLine().setLayoutY(currentFootLayoutY);

		HashMap<String, Double> newPositionProperty = new ArrowLinePositionPropertyCaculate().propertyCaculate(
				currentFootLayoutX, currentFootLayoutY, arrowLine.getHeadLayoutX(), arrowLine.getHeadLayoutX());

		arrowLine.getArrowLine().setRotate(newPositionProperty.get("angle"));
		arrowLine.getLine().setEndX(arrowLine.lengthOfLine(newPositionProperty.get("length")));
	}
}