package arrowLineAnimation;

import java.util.HashMap;

import arrowLineManager.ArrowLineManager;
import arrowLineManager.ArrowLinePositionPropertyCaculate;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.util.Duration;

public class ArrowHeadMoveToANewPosition extends Transition {
	private final ArrowLineManager arrowLine;

	private final double headDestinationX;
	private final double headDestinationY;

	private final double startHeadLayoutX;
	private final double startHeadLayoutY;

	public ArrowHeadMoveToANewPosition(ArrowLineManager arrowLine, Duration duration, double startHeadLayoutX,
			double startHeadLayoutY, double headDestinationX, double headDestinationY) {
		this.arrowLine = arrowLine;
		this.headDestinationX = headDestinationX;
		this.headDestinationY = headDestinationY;

		this.startHeadLayoutX = startHeadLayoutX;
		this.startHeadLayoutY = startHeadLayoutY;

		setCycleDuration(duration);
		setInterpolator(Interpolator.LINEAR);
	}

	@Override
	protected void interpolate(double frec) {
		final double translationDegreeX = headDestinationX - startHeadLayoutX;
		final double translationDegreeY = headDestinationY - startHeadLayoutY;

		double currentHeadLayoutX = startHeadLayoutX + translationDegreeX * frec;
		double currentHeadLayoutY = startHeadLayoutY + translationDegreeY * frec;

		HashMap<String, Double> newPositionProperty = new ArrowLinePositionPropertyCaculate().propertyCaculate(
				arrowLine.getArrowLine().getLayoutX(), arrowLine.getArrowLine().getLayoutY(), currentHeadLayoutX,
				currentHeadLayoutY);

		arrowLine.getArrowLine().setRotate(newPositionProperty.get("angle"));
		arrowLine.getLine().setEndX(arrowLine.lengthOfLine(newPositionProperty.get("length")));
	}
}