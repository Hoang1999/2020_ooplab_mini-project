package arrowLineManager;

import java.util.HashMap;

public class ArrowLinePositionPropertyCaculate {
	public HashMap<String, Double> propertyCaculate(double footLayoutX, double footLayoutY, double headLayoutX,
			double headLayoutY) {
		double lengthInX = headLayoutX - footLayoutX;
		double LengthInY = headLayoutY - footLayoutY;

		double arrowLength = Math.sqrt(lengthInX * lengthInX + LengthInY * LengthInY);

		double arrowAngle;
		if (lengthInX == 0) {
			if (LengthInY >= 0)
				arrowAngle = 90;
			else
				arrowAngle = -90;
		} else if (lengthInX > 0)
			arrowAngle = Math.atan(LengthInY / lengthInX) / Math.PI * 180;
		else
			arrowAngle = -(Math.PI - Math.atan(LengthInY / lengthInX)) / Math.PI * 180;

		HashMap<String, Double> arrowProperty = new HashMap<String, Double>();
		arrowProperty.put("length", arrowLength);
		arrowProperty.put("angle", arrowAngle);

		return arrowProperty;
	}
}
