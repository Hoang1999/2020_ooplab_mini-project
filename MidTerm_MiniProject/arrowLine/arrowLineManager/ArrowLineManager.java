package arrowLineManager;

//Only can change the property of the ArrowLine by using setPosition method
//The Line, arrowLine and Polygon of the ArrowLine cannot be change
import java.util.HashMap;

import fxmlDesign.controller.ArrowLineController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

public class ArrowLineManager {
	private String arrowLineFXML = "./../fxmlDesign/ArrowLine.fxml";

	private HBox arrowLine = null;

	private Line line;
	private Polygon arrow;

	private double footLayoutX = 0;
	private double footLayoutY = 0;
	private double headLayoutX = 0;
	private double headLayoutY = 0;

	public ArrowLineManager() {
		FXMLLoader arrowLineLoader = new FXMLLoader(getClass().getResource(arrowLineFXML));
		ArrowLineController arrowLineCont = new ArrowLineController();
		arrowLineLoader.setController(arrowLineCont);
		try {
			arrowLine = arrowLineLoader.load();
			line = arrowLineCont.getLine();
			arrow = arrowLineCont.getArrow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPosition(double footLayoutX, double footLayoutY, double headLayoutX, double headLayoutY) {
		this.footLayoutX = footLayoutX;
		this.footLayoutY = footLayoutY;
		this.headLayoutX = headLayoutX;
		this.headLayoutY = headLayoutY;

		arrowLine.setLayoutX(footLayoutX);
		arrowLine.setLayoutY(footLayoutY);

		HashMap<String, Double> positionProperty = new ArrowLinePositionPropertyCaculate().propertyCaculate(footLayoutX,
				footLayoutY, headLayoutX, headLayoutY);

		arrowLine.setRotate(positionProperty.get("angle"));

		line.setEndX(lengthOfLine(positionProperty.get("length")));
	}

	public void setStrokeWidth(double width) {
		line.setStrokeWidth(width);
	}

	public void setArrowScale(double scaleX, double scaleY) {
		arrow.setScaleX(scaleX);
		arrow.setScaleY(scaleY);
		arrow.setTranslateX(arrow.getPoints().get(0) * (1 - scaleY));
		this.setPosition(footLayoutX, footLayoutY, headLayoutX, headLayoutY);
	}

	// with the length of the arrow line, use this method to calculate the length of
	// the line of arrow line
	public double lengthOfLine(double length) {
		double lengthOfArrow = (arrow.getPoints().get(1) - arrow.getPoints().get(5)) * arrow.getScaleX();
		return length - lengthOfArrow;
	}

	public Line getLine() {
		return line;
	}

	public Polygon getArrow() {
		return arrow;
	}

	public HBox getArrowLine() {
		return arrowLine;
	}

	public double getFootLayoutX() {
		return footLayoutX;
	}

	public double getFootLayoutY() {
		return footLayoutY;
	}

	public double getHeadLayoutX() {
		return headLayoutX;
	}

	public double getHeadLayoutY() {
		return headLayoutY;
	}
}
